//
//  HomePageVC.swift
//  Fond_Memory_Maker
//
//  Created by Jason Onken on 9/6/17.
//  Copyright © 2017 JasonOnken. All rights reserved.
//

import UIKit
import CoreData

private let cellIdentifier = "CVCIdentifier"

class HomePageVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UISearchResultsUpdating, UISearchBarDelegate {
    
    // MARK: - Outlets
    
    @IBOutlet weak var memoryCollectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var borderView: UIView!
    
    // MARK: - Properties
    
    var colorTheme = ColorTheme()
    
    var backgroundColor: UIColor?
    var detailColor: UIColor?
    var fontColor: UIColor?
    
    var memoriesArray = [Memories]()
    var selectedMemory: Memories?
    var filteredMemories = [Memories]()
    
    var searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        memoryCollectionView.reloadData()
        
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        
        self.navigationItem.titleView = searchController.searchBar
        searchController.searchBar.placeholder = "Search for memory titles"
        searchController.searchBar.sizeToFit()
    
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.definesPresentationContext = false
        
        self.automaticallyAdjustsScrollViewInsets = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Load the color theme and set all the items to the correct colors
        loadTheme()
        super.viewWillAppear(animated)
        // Load in the memory array that is saved
        
        if let tbc = self.tabBarController as? CustomTabBarController {
            memoriesArray = tbc.memoriesArray
        }
        
        memoryCollectionView.reloadData()
        
        self.view.backgroundColor = backgroundColor
        navigationController?.navigationBar.barTintColor = backgroundColor
        searchController.searchBar.barTintColor = backgroundColor
        
        borderView.layer.borderWidth = 5
        borderView.layer.borderColor = detailColor?.cgColor
        navigationController?.navigationBar.tintColor = detailColor
        self.tabBarController?.tabBar.tintColor = UIColor.black
        
        for subView in searchController.searchBar.subviews {
            for subViewOne in subView.subviews {
                if let textField = subViewOne as? UITextField {
                    subViewOne.backgroundColor = detailColor
                    let placeHolderText = textField.value(forKey: "placeholderLabel") as? UILabel
                    placeHolderText?.textColor = fontColor
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Load the current theme
    func loadTheme() {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        
        if delegate.customTheme == false {
            backgroundColor = colorTheme.backgroundColor
            detailColor = colorTheme.detailsColor
            fontColor = colorTheme.fontColor
        } else {
            backgroundColor = delegate.customBackground
            detailColor = delegate.customDetail
            fontColor = delegate.customFontColor
        }
    }
    
    // Set the number of section sin the collection view
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    // Set the number of items to the memories array, or the filtered if the search controller is active
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if searchController.isActive {
            return filteredMemories.count
        }
        return memoriesArray.count
    }
    
    // Set up the collection view with all the memories
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let memoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! AlbumCollectionCell
        
        let memory: Memories
        // Set up the cells to either the memoriesArray or the search results
        if searchController.isActive {
            memory = filteredMemories[indexPath.item]
        } else {
            memory = memoriesArray[indexPath.item]
        }
        
        memoryCell.memoryImageView.image = memory.photo
        memoryCell.titleLabel.text = memory.title
        memoryCell.detailsLabel.text = memory.description
        
        // Load the color theme
        memoryCell.detailsView.backgroundColor = detailColor
        memoryCell.memoryImageView.layer.borderWidth = 1
        memoryCell.memoryImageView.layer.borderColor = detailColor?.cgColor
        memoryCell.titleLabel.textColor = fontColor
        memoryCell.detailsLabel.textColor = fontColor
        memoryCell.backgroundColor = backgroundColor
        memoryCell.dividerView.backgroundColor = fontColor
        
        return memoryCell
    }
    
    // Function for updating the search results when the search controller is active
    func updateSearchResults(for searchController: UISearchController) {
        let searchString = searchController.searchBar.text!
        
        filteredMemories = memoriesArray
        
        if searchString.isEmpty == false {
            filteredMemories = filteredMemories.filter { (memory) -> Bool in
                (memory.title.lowercased().contains(searchString.lowercased()))
            }
        }
        
        memoryCollectionView.reloadData()
    }
    
    // Function to activatethe search controller
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        memoryCollectionView.reloadData()
        searchController.isActive = false
    }
    
    // Send the selected memory to the details controller
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailSegueIdentifier" {
            let detailsVC = segue.destination as! MemoryDetailsVC
            let cell = sender as! AlbumCollectionCell
            let indexPath = self.memoryCollectionView.indexPath(for: cell)
            selectedMemory = self.memoriesArray[indexPath!.row] as Memories
            detailsVC.selectedMemory = selectedMemory
        }
    }
}
