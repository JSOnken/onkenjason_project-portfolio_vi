//
//  EditScrapbookVC.swift
//  Fond_Memory_Maker
//
//  Created by Jason Onken on 9/6/17.
//  Copyright © 2017 JasonOnken. All rights reserved.
//

import UIKit

class EditScrapbookVC: UIViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var redColorSlider: UISlider!
    @IBOutlet weak var greenColorSlider: UISlider!
    @IBOutlet weak var blueColorSlider: UISlider!
    @IBOutlet weak var editSegmentedControl: UISegmentedControl!
    @IBOutlet weak var borderColorView: UIView!
    @IBOutlet weak var detailTextView: UITextView!
    @IBOutlet weak var defaultImage: UIImageView!
    @IBOutlet weak var saveButton: UIButton!
    
    // MARK: - Properties
    
    var colorTheme = ColorTheme()
    var backgroundColor: UIColor?
    var detailColor: UIColor?
    var fontColor: UIColor?
    
    let delegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Load the color theme and set all the items to the correct theme
        loadTheme()
        
        self.view.backgroundColor = backgroundColor
        
        defaultImage.layer.borderWidth = 1
        defaultImage.layer.borderColor = detailColor?.cgColor
        borderColorView.layer.borderWidth = 5
        borderColorView.layer.borderColor = detailColor?.cgColor
        
        self.tabBarController?.tabBar.barTintColor = detailColor
        detailTextView.backgroundColor = detailColor
        detailTextView.textColor = fontColor
        saveButton.setTitleColor(detailColor, for: .normal)
        
        redColorSlider.value = Float(colorTheme.backgroundRedValue)
        greenColorSlider.value = Float(colorTheme.backgroundGreenValue)
        blueColorSlider.value = Float(colorTheme.backgroundBlueValue)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Action for changing the color based on the red slider value
    @IBAction func redSliderChanged(_ sender: Any) {
        selectedColor()
    }
    
    // Action for changing the color based on the green slider value
    @IBAction func greenSliderChanged(_ sender: Any) {
        selectedColor()
    }
    
    // Action for changing the color based on the blue slider value
    @IBAction func blueSliderChanged(_ sender: Any) {
        selectedColor()
    }
    
    // Action for the segment control to tell the app what colors we are changing
    @IBAction func segmentControlChanged(_ sender: Any) {
        // Set the sliders to the values for the background color
        if editSegmentedControl.selectedSegmentIndex == 0 {
            redColorSlider.value = colorTheme.backgroundRedValue
            greenColorSlider.value = colorTheme.backgroundGreenValue
            blueColorSlider.value = colorTheme.backgroundBlueValue
        // Set the sliders to the values for the details color
        } else if editSegmentedControl.selectedSegmentIndex == 1 {
            redColorSlider.value = colorTheme.detailsRedValue
            greenColorSlider.value = colorTheme.detailsGreenValue
            blueColorSlider.value = colorTheme.detailsBlueValue
        // Set the sliders to the values for the font color
        } else {
            redColorSlider.value = colorTheme.fontRedValue
            greenColorSlider.value = colorTheme.fontGreenValue
            blueColorSlider.value = colorTheme.fontBlueValue
        }
    }
    
    // Action to save the theme through the app
    @IBAction func saveTheme(_ sender: Any) {
        delegate.customTheme = true
        delegate.changeColors(newBackgroundColor: backgroundColor!, newDetailsColor: detailColor!, newFontColor: fontColor!)
    }
    
    // Function to load the current theme
    func loadTheme() {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        
        if delegate.customTheme == false {
            backgroundColor = colorTheme.backgroundColor
            detailColor = colorTheme.detailsColor
            fontColor = colorTheme.fontColor
        } else {
            backgroundColor = delegate.customBackground
            detailColor = delegate.customDetail
            fontColor = delegate.customFontColor
        }
    }
    
    // Fucntion for changing the color based on the slider values and the selected segment
    func selectedColor() {
        
        // Set the color to the values for the slider
        let redValue = Float(redColorSlider.value)
        let greenValue = Float(greenColorSlider.value)
        let blueValue = Float(blueColorSlider.value)
        let newColor = UIColor(colorLiteralRed: redValue, green: greenValue, blue: blueValue, alpha: 1.0)
        
        // Set the color to the background color
        if editSegmentedControl.selectedSegmentIndex == 0 {
            colorTheme.setColorValues(segment: 0, newRedValue: redValue, newGreenValue: greenValue, newBlueValue: blueValue)
            colorTheme.backgroundColor = newColor
            self.view.backgroundColor = newColor
            backgroundColor = newColor
        // Set the color to the details color
        } else if editSegmentedControl.selectedSegmentIndex == 1 {
            colorTheme.setColorValues(segment: 1, newRedValue: redValue, newGreenValue: greenValue, newBlueValue: blueValue)
            colorTheme.detailsColor = newColor
            detailTextView.backgroundColor = newColor
            defaultImage.layer.borderColor = newColor.cgColor
            borderColorView.layer.borderColor = newColor.cgColor
            saveButton.setTitleColor(newColor, for: .normal)
            self.tabBarController?.tabBar.barTintColor = newColor
            detailColor = newColor
        // Set the color to the font color
        } else {
            colorTheme.setColorValues(segment: 2, newRedValue: redValue, newGreenValue: greenValue, newBlueValue: blueValue)
            colorTheme.fontColor = newColor
            detailTextView.textColor = newColor
            self.tabBarController?.tabBar.tintColor = newColor
            fontColor = newColor
        }
    }
}

