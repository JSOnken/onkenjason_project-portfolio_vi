//
//  CommentsTableViewCell.swift
//  Fond_Memory_Maker
//
//  Created by Jason Onken on 9/8/17.
//  Copyright © 2017 JasonOnken. All rights reserved.
//

import UIKit

class CommentsTableViewCell: UITableViewCell {

    // MARK: - Outlets
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
