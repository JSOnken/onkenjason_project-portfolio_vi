//
//  ColorTheme.swift
//  Fond_Memory_Maker
//
//  Created by Jason Onken on 9/12/17.
//  Copyright © 2017 JasonOnken. All rights reserved.
//

import Foundation
import UIKit

// Class for setting the default color theme and RGB sliders
class ColorTheme {
    
    // The default colors for the app
    var backgroundColor = UIColor(colorLiteralRed: 121/255, green: 124/255, blue: 127/255, alpha: 1.0)
    var detailsColor = UIColor(colorLiteralRed: 234/255, green: 234/255, blue: 130/255, alpha: 1.0)
    var fontColor = UIColor(colorLiteralRed: 121/255, green: 124/255, blue: 127/255, alpha: 1.0)
    
    // Segment number for the edit page
    var segment = 0
    
    // The values for the sliders for each segment
    var backgroundRedValue = Float(121) / Float(255)
    var backgroundGreenValue = Float(124) / Float(255)
    var backgroundBlueValue = Float(127) / Float(255)
    
    var detailsRedValue = Float(234) / Float(255)
    var detailsGreenValue = Float(234) / Float(255)
    var detailsBlueValue = Float(130) / Float(255)
    
    var fontRedValue = Float(121) / Float(255)
    var fontGreenValue = Float(124) / Float(255)
    var fontBlueValue = Float(127) / Float(255)
    
    // Function to set the values for the color sliders after they are changed
    func setColorValues (segment: Int, newRedValue: Float, newGreenValue: Float, newBlueValue: Float) {
        // Background
        if segment == 0 {
            self.backgroundRedValue = newRedValue
            self.backgroundGreenValue = newGreenValue
            self.backgroundBlueValue = newBlueValue
        // Details
        } else if segment == 1 {
            self.detailsRedValue = newRedValue
            self.detailsGreenValue = newGreenValue
            self.detailsBlueValue = newBlueValue
        // Font
        } else {
            self.fontRedValue = newRedValue
            self.fontGreenValue = newGreenValue
            self.fontBlueValue = newBlueValue
        }
    }
    
}
