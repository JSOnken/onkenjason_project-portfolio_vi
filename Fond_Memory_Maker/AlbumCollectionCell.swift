//
//  AlbumCollectionCell.swift
//  Fond_Memory_Maker
//
//  Created by Jason Onken on 9/6/17.
//  Copyright © 2017 JasonOnken. All rights reserved.
//

import UIKit

class AlbumCollectionCell: UICollectionViewCell {
    
    // MARK: - Ooutlets
    
    @IBOutlet weak var memoryImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailsLabel: UITextView!
    @IBOutlet weak var dividerView: UIView!
    @IBOutlet weak var detailsView: UIView!
    
}
