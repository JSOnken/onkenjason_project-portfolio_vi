//
//  AccountPageVC.swift
//  Fond_Memory_Maker
//
//  Created by Jason Onken on 9/6/17.
//  Copyright © 2017 JasonOnken. All rights reserved.
//

import UIKit

private let cellIdentifier = "sharedCellIdentifier"

class AccountPageVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    // MARK: - Outlets
    
    @IBOutlet weak var accountLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var sharedWithLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var createAccountButton: UIButton!
    @IBOutlet weak var sharedTableView: UITableView!
    
    // MARK: - Properties
    
    var backgroundColor: UIColor?
    var detailColor: UIColor?
    var fontColor: UIColor?
    
    var colorTheme = ColorTheme()
    var sharedUsers = [String]()
    var userToAdd: String?
    
    let delegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        nameTextField.text = delegate.userName
        emailTextField.text = delegate.userEmail
        passwordTextField.text = "*********"
        if delegate.userEmail == "" {
            createAccountButton.isHidden = false
            addButton.isEnabled = false
            editButton.isHidden = true
            saveButton.isHidden = true
        } else {
            createAccountButton.isHidden = true
        }
        
        self.nameTextField.delegate = self
        self.emailTextField.delegate = self
        self.passwordTextField.delegate = self
        self.confirmPasswordTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Load the current color theme and set all the items to the theme
        loadTheme()
        
        self.view.backgroundColor = backgroundColor
        accountLabel.textColor = detailColor
        infoLabel.textColor = detailColor
        sharedWithLabel.textColor = detailColor
        nameTextField.backgroundColor = detailColor
        nameTextField.textColor = fontColor
        emailTextField.backgroundColor = detailColor
        emailTextField.textColor = fontColor
        passwordTextField.backgroundColor = detailColor
        passwordTextField.textColor = fontColor
        confirmPasswordTextField.backgroundColor = detailColor
        confirmPasswordTextField.textColor = fontColor
        editButton.setTitleColor(detailColor, for: .normal)
        addButton.backgroundColor = detailColor
        addButton.setTitleColor(fontColor, for: .normal)
        saveButton.setTitleColor(detailColor, for: .normal)
        createAccountButton.setTitleColor(detailColor, for: .normal)
        
        addButton.layer.cornerRadius = 5
        sharedTableView.tableFooterView = UIView()
        sharedTableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Funciton for loading the current theme
    func loadTheme() {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        
        if delegate.customTheme == false {
            backgroundColor = colorTheme.backgroundColor
            detailColor = colorTheme.detailsColor
            fontColor = colorTheme.fontColor
        } else {
            backgroundColor = delegate.customBackground
            detailColor = delegate.customDetail
            fontColor = delegate.customFontColor
        }
    }
    
    // Function to dismiss the keyboard when return is pressed
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    // Function to dismiss the keyboard when the user taps outside the textfield
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // Fucntion to validate the correct email format
    func validateEmail(enteredEmail:String) -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
    }
    
    // Set the number of items to the shared users
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sharedUsers.count
    }
    
    // Load the table cells with the emails of the shared users
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sharedWithCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! SharedTableViewCell
        
        sharedWithCell.sharedWithLabel.text = sharedUsers[indexPath.item]
        
        // Set the color theme for the cells
        sharedWithCell.layer.cornerRadius = 5
        sharedWithCell.backgroundColor = detailColor
        sharedWithCell.sharedWithLabel.textColor = fontColor
        
        return sharedWithCell
    }
    
    // Set the height for the cells
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    // Action for adding an account to share memories with
    @IBAction func addButtonPressed(_ sender: Any) {
        // Show the alert controller to type in the email of the user
        let alertController = UIAlertController(title: "Share Your Memories", message: "Please type in the email of the person to share with.", preferredStyle: .alert)
        
        // Add the person to the list
        let addPerson = UIAlertAction(title: "Add", style: .default) { (_) in
            if let userTextField = alertController.textFields?[0] {
                self.userToAdd = userTextField.text!
            }
            if (self.userToAdd?.isEmpty)! {
                return
            } else {
                self.sharedUsers.append(self.userToAdd!)
                self.sharedTableView.reloadData()
            }
        }
        
        // Cancel the action
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addTextField(configurationHandler: { (textField) in
            textField.placeholder = "Enter user email"
        })
        
        alertController.addAction(addPerson)
        alertController.addAction(cancel)
        
        present(alertController, animated: true, completion: nil)

    }
    
    // Action for editing the user's account information
    @IBAction func editButtonPressed(_ sender: Any) {
        confirmPasswordTextField.isHidden = false
        editButton.isHidden = true
        saveButton.isHidden = false
        confirmPasswordTextField.isEnabled = true
        nameTextField.isEnabled = true
        emailTextField.isEnabled = true
        passwordTextField.isEnabled = true
        
    }
    
    // Action for saving the changes to the user's account
    @IBAction func saveButtonPressed(_ sender: Any) {
        let userName = nameTextField.text
        let userEmail = emailTextField.text
        let userPassword = passwordTextField.text
        let confirmPassword = confirmPasswordTextField.text
        
        // Make sure that all the fields are filled in properly
        // If they are then save the changes
        // If they are not then tell the user the error
        if ((userName?.isEmpty)! || (userEmail?.isEmpty)! || (userPassword?.isEmpty)! || (confirmPassword?.isEmpty)!) {
            displayAlert(title: "Alert!", errorMessage: "Please fill in all fields.")
            return
        } else if userPassword != confirmPassword {
            displayAlert(title: "Alert!", errorMessage: "Passwords do not match.")
            return
        } else if validateEmail(enteredEmail: emailTextField.text!) == false {
            displayAlert(title: "Alert!", errorMessage: "Email was not entered in the correct format.")
        }
        else {
            let delegate = UIApplication.shared.delegate as! AppDelegate
            delegate.userName = userName!
            delegate.userEmail = userEmail!
            delegate.userPassword = userPassword!
            
            displayAlert(title: "Updated!", errorMessage: "Your account has been updated.")
            saveButton.isHidden = true
            editButton.isHidden = false
            confirmPasswordTextField.isHidden = true
        }
    }
    
    // Function for displaying the alert controller
    func displayAlert(title: String, errorMessage: String) {
        let alertController = UIAlertController(title: title, message: errorMessage, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alertController.addAction(ok)
        
        self.present(alertController, animated: true, completion: nil)
    }

}
