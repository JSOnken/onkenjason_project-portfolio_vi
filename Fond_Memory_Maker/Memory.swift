//
//  Memory.swift
//  Fond_Memory_Maker
//
//  Created by Jason Onken on 9/7/17.
//  Copyright © 2017 JasonOnken. All rights reserved.
//

import Foundation
import UIKit

// Calss for creating memories
class Memories {

    // MARK: - Properties
    
    var title: String
    var description: String
    var photo: UIImage
    var commentsArray = [(name: String, comment: String)]()
    
    init(title: String, description: String, photo: UIImage) {
        self.title = title
        self.description = description
        self.photo = photo
    }
    
    // Function for adding comments to a selected memory
    func addComment(name: String, comment: String) {
        commentsArray.append(name: name, comment: comment)
    }
    
}
