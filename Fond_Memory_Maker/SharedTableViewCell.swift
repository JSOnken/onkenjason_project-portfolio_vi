//
//  SharedTableViewCell.swift
//  Fond_Memory_Maker
//
//  Created by Jason Onken on 9/20/17.
//  Copyright © 2017 JasonOnken. All rights reserved.
//

import UIKit

class SharedTableViewCell: UITableViewCell {

    // MARK: - Outlets
    
    @IBOutlet weak var sharedWithLabel: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
