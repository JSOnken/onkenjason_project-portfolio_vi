//
//  CustomTabBarController.swift
//  Fond_Memory_Maker
//
//  Created by Jason Onken on 9/7/17.
//  Copyright © 2017 JasonOnken. All rights reserved.
//

import Foundation
import UIKit

// Custom tab bar controller class to hold a copy of the memories array for passing between view controllers
class CustomTabBarController: UITabBarController {
    
    var memoriesArray = [Memories]()
}
