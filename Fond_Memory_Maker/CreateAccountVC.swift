//
//  CreateAccountVC.swift
//  Fond_Memory_Maker
//
//  Created by Jason Onken on 9/6/17.
//  Copyright © 2017 JasonOnken. All rights reserved.
//

import UIKit
import CoreData

class CreateAccountVC: UIViewController, UITextFieldDelegate {

    // MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmTextField: UITextField!
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    
    // MARK: - Properties
    var backgroundColor: UIColor?
    var detailColor: UIColor?
    var fontColor: UIColor?
    
    var colorTheme = ColorTheme()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        continueButton.isHidden = true
        self.nameTextField.delegate = self
        self.emailTextField.delegate = self
        self.passwordTextField.delegate = self
        self.confirmTextField.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Load the color theme and set all the items to the correct theme
        loadTheme()
        self.view.backgroundColor = backgroundColor
        
        nameTextField.backgroundColor = detailColor
        emailTextField.backgroundColor = detailColor
        passwordTextField.backgroundColor = detailColor
        emailTextField.backgroundColor = detailColor
        createButton.setTitleColor(detailColor, for: .normal)
        backButton.setTitleColor(detailColor, for: .normal)
        continueButton.setTitleColor(detailColor, for: .normal)
        titleLabel.textColor = detailColor
        subTitleLabel.textColor = detailColor
        
        nameTextField.textColor = fontColor
        emailTextField.textColor = fontColor
        passwordTextField.textColor = fontColor
        confirmTextField.textColor = fontColor
    }
    
    // Function for loading the current theme
    func loadTheme() {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        
        if delegate.customTheme == false {
            backgroundColor = colorTheme.backgroundColor
            detailColor = colorTheme.detailsColor
            fontColor = colorTheme.fontColor
        } else {
            backgroundColor = delegate.customBackground
            detailColor = delegate.customDetail
            fontColor = delegate.customFontColor
        }
    }
    
    // Make sure the keyboard hides when the user presses return
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    // Make sure the keyboard hides when the user taps outside the textfield
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // Make sure that the user enters an email in the correct format
    func validateEmail(enteredEmail:String) -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
    }
    
    // Action for creating an account
    @IBAction func createButtonPressed(_ sender: Any) {
        let userName = nameTextField.text
        let userEmail = emailTextField.text
        let userPassword = passwordTextField.text
        let confirmPassword = confirmTextField.text
        
        // Make sure that all the fields are filled in properly and that the passwords match
        // If they do save the account and log the user in
        // If not let the user know where the error is
        if ((userName?.isEmpty)! || (userEmail?.isEmpty)! || (userPassword?.isEmpty)! || (confirmPassword?.isEmpty)!) {
            displayAlert(title: "Alert!", errorMessage: "Please fill in all fields.")
            return
        } else if userPassword != confirmPassword {
            displayAlert(title: "Alert!", errorMessage: "Passwords do not match.")
            return
        } else if validateEmail(enteredEmail: emailTextField.text!) == false {
            displayAlert(title: "Alert!", errorMessage: "Email is not in the correct format.")
        } else {
            let delegate = UIApplication.shared.delegate as! AppDelegate
//            newUser.setValue(userName, forKey: "name")
//            newUser.setValue(userEmail, forKey: "email")
//            newUser.setValue(userPassword, forKey: "password")
            delegate.userName = userName!
            delegate.userEmail = userEmail!
            delegate.userPassword = userPassword!
            
            displayAlert(title: "User Created", errorMessage: "Your account has been created.")
            continueButton.isHidden = false
            createButton.isHidden = true
        }
    }
    
    // Action to dismiss the view back to the sign in page
    @IBAction func backButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // Fucntion for display the alert controller
    func displayAlert(title: String, errorMessage: String) {
        let alertController = UIAlertController(title: title, message: errorMessage, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alertController.addAction(ok)
        
        self.present(alertController, animated: true, completion: nil)
    }
}
