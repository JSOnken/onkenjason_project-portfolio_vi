//
//  MemoryDetailsVC.swift
//  Fond_Memory_Maker
//
//  Created by Jason Onken on 9/8/17.
//  Copyright © 2017 JasonOnken. All rights reserved.
//

import UIKit
import Social

private var cellIdentifier = "ComentCellIdentifier"

class MemoryDetailsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Outlets
    
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var selectedImage: UIImageView!
    @IBOutlet weak var detailsView: UIView!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var commentsView: UIView!
    @IBOutlet weak var commentsTableView: UITableView!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var commentCountLabel: UILabel!
    @IBOutlet weak var commentButton: UIButton!
    
    // MARK: - Properties
    
    var commentsCount = 0
    var newComment = ""
    var selectedMemory: Memories!
    
    var colorTheme = ColorTheme()
    var backgroundColor: UIColor?
    var detailColor: UIColor?
    var fontColor: UIColor?
    
    let delegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if let count = selectedMemory?.commentsArray.count {
            commentsCount = count
        }
        selectedImage.image = selectedMemory.photo
        titleLabel.text = selectedMemory?.title
        detailsLabel.text = selectedMemory?.description
        commentsTableView.reloadData()
        
        if let count = selectedMemory?.commentsArray.count {
            commentsCount = count
            commentCountLabel.text = "\(count) Comments"
        } else {
            commentsCount = 0
            commentCountLabel.text = "0 Comments"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadTheme()
        
        self.view.backgroundColor = backgroundColor
        navigationController?.navigationBar.barTintColor = backgroundColor
        navigationController?.navigationBar.tintColor = detailColor
        borderView.layer.borderWidth = 5
        borderView.layer.borderColor = detailColor?.cgColor
        selectedImage.layer.borderWidth = 1
        selectedImage.layer.borderColor = detailColor?.cgColor
        titleView.backgroundColor = detailColor
        detailsView.backgroundColor = detailColor
        commentsView.backgroundColor = detailColor
        commentsTableView.backgroundColor = detailColor
        if commentsCount == 0 {
            commentsTableView.isHidden = true
        }
        
        titleLabel.textColor = fontColor
        detailsLabel.textColor = fontColor
        commentCountLabel.textColor = fontColor
        commentButton.setTitleColor(fontColor, for: .normal)
        commentsTableView.reloadData()
        
    }
    
    // Function for loading the theme
    func loadTheme() {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        
        if delegate.customTheme == false {
            backgroundColor = colorTheme.backgroundColor
            detailColor = colorTheme.detailsColor
            fontColor = colorTheme.fontColor
        } else {
            backgroundColor = delegate.customBackground
            detailColor = delegate.customDetail
            fontColor = delegate.customFontColor
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentsCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let commentCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! CommentsTableViewCell

        commentCell.nameLabel?.text = selectedMemory?.commentsArray[indexPath.item].name
        commentCell.commentLabel?.text = selectedMemory?.commentsArray[indexPath.item].comment
        commentCell.backgroundColor = detailColor
        commentCell.nameLabel.textColor = fontColor
        commentCell.commentLabel.textColor = fontColor
        
        return commentCell
    }
    
    // Action for adding a comment to the memory
    @IBAction func commentButtonPressed(_ sender: Any) {
        let alertController = UIAlertController(title: "New Comment", message: "Please enter your comment.", preferredStyle: .alert)
        let saveComment = UIAlertAction(title: "Comment", style: .default) { (_) in
            if let comment = alertController.textFields?[0] {
                self.newComment = comment.text!
            } else {
                self.newComment = ""
            }
            self.commentsCount += 1
            self.commentCountLabel.text = "\(self.commentsCount) Comments"
            self.selectedMemory?.addComment(name: self.delegate.userName, comment: self.newComment)
            self.commentsTableView.reloadData()
            self.commentsTableView.isHidden = false
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addTextField(configurationHandler: { (textField) in
            textField.placeholder = "Enter comment"
        })
        
        alertController.addAction(saveComment)
        alertController.addAction(cancel)
        
        present(alertController, animated: true, completion: nil)
        
        commentsTableView.reloadData()
    }
    
    // Action to allow the user to post to Facebook if the user authorizes and has an account
    @IBAction func facebookButtonPressed(_ sender: Any) {
        let facebookVC = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
        facebookVC?.add(selectedMemory.photo)
        facebookVC?.setInitialText("\(selectedMemory.title)\n\(selectedMemory.description)")
        self.present(facebookVC!, animated: true, completion: nil)
    }
    
    // Action to allow the user to post to Twitter if the user authorizes and has an account
    @IBAction func twitterButtonPressed(_ sender: Any) {
        let twitterVC = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
                    twitterVC?.add(selectedMemory.photo)
        twitterVC?.setInitialText("\(selectedMemory.title)\n\(selectedMemory.description)")
        self.present(twitterVC!, animated: true, completion: nil)
    }
    
}
