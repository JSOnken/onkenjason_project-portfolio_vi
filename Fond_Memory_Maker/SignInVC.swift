//
//  SignInVC.swift
//  Fond_Memory_Maker
//
//  Created by Jason Onken on 9/6/17.
//  Copyright © 2017 JasonOnken. All rights reserved.
//

import UIKit
import CoreData

class SignInVC: UIViewController, UITextFieldDelegate {
    
    // MARK: - Outlets
    
    @IBOutlet weak var logoView: UIView!
    @IBOutlet weak var imageView0: UIImageView!
    @IBOutlet weak var imageView1: UIImageView!
    @IBOutlet weak var imageView2: UIImageView!
    @IBOutlet weak var emailTextView: UITextField!
    @IBOutlet weak var passwordTextView: UITextField!
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var guestButton: UIButton!
    @IBOutlet weak var titleLabel0: UILabel!
    @IBOutlet weak var titleLabel1: UILabel!
    
    // MARK: - Properties
    
    var colorTheme = ColorTheme()
    
    var backgroundColor: UIColor?
    var detailColor: UIColor?
    var fontColor: UIColor?
    
    var registeredUsers = [(email: String, password: String)]()
    
//    private var managedContext: NSManagedObjectContext!
//    private var entityDescription: NSEntityDescription!
//    private var users: NSManagedObject!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
//        managedContext = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
//        entityDescription = NSEntityDescription.entity(forEntityName: "User", in: managedContext)
//        users = NSManagedObject(entity: entityDescription, insertInto: managedContext)
//        loadData()
        
        self.emailTextView.delegate = self
        self.passwordTextView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Load the color theme and set all the items to the theme
        loadTheme()
        self.view.backgroundColor = backgroundColor
        
        logoView.layer.borderWidth = 5
        logoView.layer.borderColor = detailColor?.cgColor
        imageView0.layer.borderWidth = 1
        imageView0.layer.borderColor = detailColor?.cgColor
        imageView1.layer.borderWidth = 1
        imageView1.layer.borderColor = detailColor?.cgColor
        imageView2.layer.borderWidth = 1
        imageView2.layer.borderColor = detailColor?.cgColor
        
        emailTextView.backgroundColor = detailColor
        passwordTextView.backgroundColor = detailColor
        titleLabel0.textColor = detailColor
        titleLabel1.textColor = detailColor
        signInButton.setTitleColor(detailColor, for: .normal)
        guestButton.setTitleColor(detailColor, for: .normal)
        createButton.setTitleColor(detailColor, for: .normal)
        
        emailTextView.textColor = fontColor
        passwordTextView.textColor = fontColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Function for loading the current color theme
    func loadTheme() {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        
        if delegate.customTheme == false {
            backgroundColor = colorTheme.backgroundColor
            detailColor = colorTheme.detailsColor
            fontColor = colorTheme.fontColor
        } else {
            backgroundColor = delegate.customBackground
            detailColor = delegate.customDetail
            fontColor = delegate.customFontColor
        }
    }
    
    // Make sure the textfield hides when return is pressed
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    // Make sure the text field hides when the user taps outside of a textfield
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
//    func loadData() {
//        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
//        
//        do {
//            if let fetchedUsers = try managedContext.fetch(fetchRequest) as? [NSManagedObject] {
//                for user in fetchedUsers {
//                    let userEmail = user.value(forKey: "email") as! String
//                    let userPassword = user.value(forKey: "password") as! String
//                    registeredUsers.append((email: userEmail, password: userPassword))
//                }
//            }
//        } catch  {
//            print("Load Failed")
//        }
//    }
    
    // Function to check if a user has an account
    func checkUsers(registeredUsers:[(String, String)], userToCheck:(String, String))  -> Bool {
        // Set the user to chack and loop through all the accounts to verify if it is a registered account
        let (u1, u2) = userToCheck
        for (c1, c2) in registeredUsers {
            if u1 == c1 && u2 == c2 {
                return true
            }
        }
        // Return false if the account is not found
        return false
    }
    
    // Action for when the user signs in
    @IBAction func signInButtonPressed(_ sender: Any) {
        let enteredEmail = emailTextView.text
        let enteredPassword = passwordTextView.text
        // Check if the user has a registered account
        let registered = checkUsers(registeredUsers: registeredUsers, userToCheck: (enteredEmail!, enteredPassword!))
        
        // Present the alert that the user has signed in
        if registered == true {
            let alertController = UIAlertController(title: "Signed In", message: "Sign in was successful.", preferredStyle: .alert)
            
            let goToHomePage = UIAlertAction(title: "OK", style: .default, handler: { action in self.performSegue(withIdentifier: "signInIdentifier", sender: self)
            })
            
            alertController.addAction(goToHomePage)
            
            self.present(alertController, animated: true, completion: nil)
        // Present the error if the account could not be found
        } else {
            let alertController = UIAlertController(title: "Alert!", message: "Email or Password do not match. Please try again.", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            alertController.addAction(ok)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
}
