//
//  AddMemoryVC.swift
//  Fond_Memory_Maker
//
//  Created by Jason Onken on 9/6/17.
//  Copyright © 2017 JasonOnken. All rights reserved.
//

import UIKit
import CoreData

class AddMemoryVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UITextViewDelegate {
    
    // MARK: - Outlets
    
    @IBOutlet weak var memoryImage: UIImageView?
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var titleTextView: UIView!
    @IBOutlet weak var detailsTextView: UITextView!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var photosButton: UIButton!
    
    // MARK: - Properties
    
    var managedObjectContext: NSManagedObjectContext!
    
    let imagePicker = UIImagePickerController()
    var colorTheme = ColorTheme()
    
    var backgroundColor: UIColor?
    var detailColor: UIColor?
    var fontColor: UIColor?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        imagePicker.delegate = self
        self.titleTextField.delegate = self
        self.detailsTextView.delegate = self
        managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Load the color theme and set all the items to the correct colors
        loadTheme()
        
        self.view.backgroundColor = backgroundColor
        
        self.borderView.layer.borderWidth = 5
        self.borderView.layer.borderColor = detailColor?.cgColor
        memoryImage?.layer.borderWidth = 1
        memoryImage?.layer.borderColor = detailColor?.cgColor
        memoryImage?.backgroundColor = detailColor
        detailsTextView.backgroundColor = detailColor
        titleTextView.backgroundColor = detailColor
        
        detailsTextView.textColor = fontColor
        titleTextField.textColor = fontColor
        saveButton.setTitleColor(detailColor, for: .normal)
        cameraButton.setTitleColor(detailColor, for: .normal)
        photosButton.setTitleColor(detailColor, for: .normal)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Function to load the current theme
    func loadTheme() {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        
        if delegate.customTheme == false {
            backgroundColor = colorTheme.backgroundColor
            detailColor = colorTheme.detailsColor
            fontColor = colorTheme.fontColor
        } else {
            backgroundColor = delegate.customBackground
            detailColor = delegate.customDetail
            fontColor = delegate.customFontColor
        }
    }
    
    // Hide the keyboard when the user hits return
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    // Hide the keyboard when the user taps outside the textfield
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // Function for setting the user selected image to the memory
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            memoryImage?.contentMode = .scaleAspectFit
            memoryImage?.image = image
        }
        dismiss(animated: true, completion: nil)
    }
    
    // Function for dismissing the controller
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    // Function for creating a new memory and saving it to core data
    func createMemory(image: UIImage, title: String, details: String) {
        // Create a new managed object context and save the memory info to it
        let memoryItem = Memory(context: managedObjectContext)
        memoryItem.photo = NSData(data: UIImageJPEGRepresentation(image, 0.3)!)
        memoryItem.title = title
        memoryItem.details = details
        
        // Save the memory, or print out the error
        do {
            try managedObjectContext.save()
        } catch {
            print("Could not save data \(error.localizedDescription)")
        }
    }
    
    // Action to save the memory when the user taps save
    @IBAction func saveButtonPressed(_ sender: AnyObject) {
        // Make sure the user has an image selected before saving
        if let imageToSave = memoryImage?.image {
            let newMemory = Memories.init(title: titleTextField.text!, description: detailsTextView.text, photo: (imageToSave))
            
            // Save the memory back to the memoriesArray
            if let tbc = self.tabBarController as? CustomTabBarController {
                tbc.memoriesArray.append(newMemory)
            }
            
            // Set the view back to the default values
            memoryImage?.image = nil
            titleTextField.text = nil
            detailsTextView.text = "Details..."
            
            createMemory(image: imageToSave, title: titleTextField.text!, details: detailsTextView.text)
        
        // Present an alert if the user did not select an image
        } else {
            let alertController = UIAlertController(title: "Alert!", message: "You must select a photo defore saving.", preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            alertController.addAction(okAction)
            
            present(alertController, animated: true, completion: nil)
        }
    }
    
    // Action for opening the camera if the user authorized to take a photo to use
    @IBAction func openCameraPressed(_ sender: AnyObject) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    // Action for opening up the photo library if the user authorized to select a photo to use
    @IBAction func openPhotosPressed(_ sender: AnyObject) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
}
